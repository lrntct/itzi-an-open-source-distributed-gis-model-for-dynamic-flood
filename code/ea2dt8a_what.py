#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
AUTHOR(S): Laurent Courty

PURPOSE:    Output values of a STRDS at vector points

COPYRIGHT: (C) 2016-2017 by Laurent Courty

            This program is free software; you can redistribute it and/or
            modify it under the terms of the GNU General Public License
            as published by the Free Software Foundation; either version 2
            of the License, or (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.
"""
from __future__ import division
import sys
import grass.script as gscript
import csv

COL_NAMES = ["start_time", "end_time"] + [i for i in range(1, 10)]
CSV_OUTPUT_NAME = "../tables/ea2dt8a_17.1.csv"
STRDS_H = "out_v17.1_h@EA2DT8A_res17.1"
VECTOR_MAP = "test8output2@EA2DT8A"

def main():
    # Read results
    csv_file = gscript.read_command("t.rast.what", points=VECTOR_MAP, strds=STRDS_H,
                         output="-", separator='comma', layout='col').splitlines()
    res_dict = csv.DictReader(csv_file, fieldnames=COL_NAMES)
    # remove end columns
    results = []
    for res_line in res_dict:
        results.append({k:v for k, v in res_line.iteritems() if k != 'end_time'})
    # write results in file
    with open(CSV_OUTPUT_NAME, 'w') as csvfile:
        # remove "end" from keys
        COL_NAMES.remove("end_time")
        writer = csv.DictWriter(csvfile, fieldnames=COL_NAMES)
        writer.writeheader()
        for res_line in results:
            writer.writerow(res_line)
    return None


if __name__ == "__main__":
    options, flags = gscript.parser()
    sys.exit(main())
