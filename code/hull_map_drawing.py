import os
import sys
from datetime import datetime, timedelta
import grass.script as grass
from grass.pygrass.gis.region import Region
import grass.temporal as tgis

STRDS = "out_nvar_drainvar_h@itzi_results_17.1"
DEM = 'dem05m@hull'
SHADED = 'dem05m_shaded@hull'
HMAX = 'out_nvar_drainvar_h_max@itzi_results_17.1'
FLOODED_EA = 'ExtentEAOnly@hull'
FLOODED_COUNCIL = 'ExtentCouncilOnly@hull'
FLOODED_BOTH = 'ExtentBoth@hull'
FLOODED_MAPS = {'ea': FLOODED_EA, 'council': FLOODED_COUNCIL, 'both': FLOODED_BOTH}
FLOODED_COLORS = {'ea': "27:158:119", 'council': "217:95:2", 'both': "117:112:179"}
FLOODED_LABELS = {'ea': "UK EA", 'council': "Hull City Council", 'both': "Both"}
FRICTION = 'n_glc_20161031@hull'
LAND_COVER = 'Land_cover@hull'
TEXTURE = 'texture_avg60_250m@hull'

TEXT_COLOR = 'black'
BGCOLOR = 'white'
FONT = 'Liberation Sans:Regular'
TEXTSIZE = 5
FONTSIZE = 70
TEXTSIZE2 = 8
FONTSIZE2 = 85
TEXTSIZE_V = 4
FONTSIZE_V = 30


region = Region()
xr = region.cols
yr = region.rows

# set environment variables
os.environ['GRASS_RENDER_IMMEDIATE'] = "cairo"
os.environ['GRASS_RENDER_WIDTH'] = str(xr)
os.environ['GRASS_RENDER_HEIGHT'] = str(yr)
os.environ['GRASS_RENDER_FILE_COMPRESSION'] = "9"
os.environ['GRASS_RENDER_FILE_READ'] = "TRUE"
os.environ['GRASS_FONT'] = FONT


def draw_dem(file_name):
    os.environ['GRASS_RENDER_FILE'] = file_name
    # dem
    grass.run_command("d.rast", quiet=True, map=DEM)
    # legend
    grass.run_command("d.legend", raster=DEM, quiet=True, at="40,90,1,3",
                      color=TEXT_COLOR, range='0,85', title="Elevation in metres",
                      flags='',
                      fontsize=FONTSIZE, font=FONT, title_fontsize=FONTSIZE2)
    # scale
    grass.run_command("d.barscale", at="48,3", quiet=True, bgcolor=BGCOLOR,
                      fontsize=FONTSIZE, color=TEXT_COLOR)


def draw_land_cover(file_name):
    os.environ['GRASS_RENDER_FILE'] = file_name
    # friction
    grass.run_command("d.rast", quiet=True, map=LAND_COVER)
    # legend
    grass.run_command('d.legend', raster=LAND_COVER, quiet=True, at="0,30,1,3",
                      color=TEXT_COLOR, #title="Land cover classes",
                      flags='c', use='10,20,30,60,80',
                      title_fontsize=FONTSIZE2, fontsize=FONTSIZE, font=FONT,)
    # scale
    grass.run_command("d.barscale", at="48,3", quiet=True, bgcolor=BGCOLOR,
                      fontsize=FONTSIZE, color=TEXT_COLOR, flags='n')


def draw_texture(file_name):
    os.environ['GRASS_RENDER_FILE'] = file_name
    # friction
    grass.run_command("d.rast", quiet=True, map=TEXTURE)
    # legend
    grass.run_command('d.legend', raster=TEXTURE, quiet=True, at="0,30,1,3",
                      color=TEXT_COLOR, #title="Soil texture classes",
                      use='3,4,5,7,8',
                      flags='c', title_fontsize=FONTSIZE2,
                      fontsize=FONTSIZE, font=FONT)
    # scale
    grass.run_command("d.barscale", at="48,3", quiet=True, bgcolor=BGCOLOR,
                      fontsize=FONTSIZE, color=TEXT_COLOR, flags='n')


def draw_friction(file_name):
    os.environ['GRASS_RENDER_FILE'] = file_name
    # friction
    grass.run_command("d.rast", quiet=True, map=FRICTION)
    # legend
    grass.run_command('d.legend', raster=FRICTION, quiet=True, at="40,90,1,3",
                      color=TEXT_COLOR, title="Manning's n", title_fontsize=FONTSIZE2,
                      fontsize=FONTSIZE, font=FONT, use='0.019,0.03,0.04,0.05')
    # scale
    grass.run_command("d.barscale", at="48,3", quiet=True, bgcolor=BGCOLOR,
                      fontsize=FONTSIZE, color=TEXT_COLOR)


def draw_flooded_areas(file_name):
    os.environ['GRASS_RENDER_FILE'] = file_name
    # shaded relief
    grass.run_command("d.rast", quiet=True, map=SHADED)
    # flooded areas
    for k, flooded_map in FLOODED_MAPS.iteritems():
        grass.run_command('d.vect', map=flooded_map,
                          fill_color=FLOODED_COLORS[k],
                          legend_label=FLOODED_LABELS[k],
                          quiet=True)
    # scale
    grass.run_command("d.barscale", at="50,3", quiet=True,
                      color=TEXT_COLOR, bgcolor=BGCOLOR)
    # legend
    legend_file = os.environ['GRASS_LEGEND_FILE']
    grass.run_command('d.legend.vect', at="0,95",
                      title="Identified flooded areas",
                      symbol_size=35, border_color=None, input=legend_file,
                      border_width=1, fontsize=FONTSIZE, title_fontsize=FONTSIZE2)


def draw_maxdepth(file_name):
    os.environ['GRASS_RENDER_FILE'] = file_name

    # water depth
    grass.run_command("d.rast", quiet=True, map=HMAX)

    # legend
    grass.run_command("d.legend", raster=HMAX, quiet=True,
                      at="15,90,1,8", range='0,1', title="Water depth (m)",
                      fontsize=FONTSIZE2, font=FONT)
    # scale
    grass.run_command("d.barscale", at="48,3", quiet=True, bgcolor=BGCOLOR,
                  fontsize=FONTSIZE2, color=TEXT_COLOR)


def draw_series(prefix):
    # get map list
    tgis.init()
    cols = ['id', 'start_time']
    strds = tgis.open_stds.open_old_stds(STRDS, 'strds')
    maps = strds.get_registered_maps(columns=','.join(cols), order='start_time')
    # draw
    count = 0
    for m in maps:
        count +=1
        # date
        date = timedelta(seconds=m[1])
        if str(date) in ['9:00:00', '12:00:00', '15:00:00', '19:00:00', '23:00:00']:
            str_hour = str(date).split(':')[0].zfill(2)
            file_name = prefix+str_hour+'.png'
            os.environ['GRASS_RENDER_FILE'] = file_name
            print(file_name+'...')
            # water depth
            grass.run_command("d.rast", quiet=True, map=m[0])
            # time
            grass.run_command("d.text", text=u"Time: {}".format(str(date)), quiet=True,
                              bgcolor=BGCOLOR, at="1,5", color=TEXT_COLOR, size=TEXTSIZE2)
            # legend
            grass.run_command("d.legend", raster=HMAX, quiet=True,
                              at="15,90,1,8", range='0,1', title="Water depth (m)",
                              fontsize=FONTSIZE2, font=FONT)
            # scale
            grass.run_command("d.barscale", at="48,3", quiet=True, bgcolor=BGCOLOR,
                          fontsize=FONTSIZE2, color=TEXT_COLOR)


def main():
    #~ draw_dem('hull_dem.png')
    #~ draw_flooded_areas('hull_flooded.png')  # no transparency, legend broken
    draw_maxdepth('hull_hmax.png')
    #~ draw_friction('hull_friction.png')
    #~ draw_texture('hull_texture.png')
    #~ draw_land_cover('hull_land_cover.png')
    #~ draw_series('hull_h')


if __name__ == "__main__":
    sys.exit(main())
