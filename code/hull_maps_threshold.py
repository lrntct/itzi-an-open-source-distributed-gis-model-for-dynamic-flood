#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
AUTHOR(S): Laurent Courty

PURPOSE:    Use r.mapcalc to calculate depth threshold maps.
            Above threshold is 1, under is 0

COPYRIGHT: (C) 2016-2017 by Laurent Courty

            This program is free software; you can redistribute it and/or
            modify it under the terms of the GNU General Public License
            as published by the Free Software Foundation; either version 2
            of the License, or (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.
"""
from __future__ import division
import sys
import grass.script as gscript

H_MAP = "out_nvar_drainvar_h_max"
THRES = [0.05, 0.10, 0.20, 0.30]
OVR = True


def main():
    for thres in THRES:
        res_map = H_MAP.format(suffix="_{}".format(thres))
        exp = "{r} = if({h} >= {t}, 1, 0)".format(r=res_map, h=H_MAP, t=thres)
        gscript.run_command("r.mapcalc", expression=exp, overwrite=OVR)


if __name__ == "__main__":
    options, flags = gscript.parser()
    sys.exit(main())
