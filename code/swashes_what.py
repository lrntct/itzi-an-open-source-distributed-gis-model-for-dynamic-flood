#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
AUTHOR(S): Laurent Courty

PURPOSE:   Generate CSV with following columns: dx, topo, wse, itzi

COPYRIGHT: (C) 2016-2017 by Laurent Courty

            This program is free software; you can redistribute it and/or
            modify it under the terms of the GNU General Public License
            as published by the Free Software Foundation; either version 2
            of the License, or (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.
"""
from __future__ import division
import sys, io
import grass.script as gscript
import csv
import pandas as pd
import numpy as np

COL_NAMES = ['x', 'y', 'site_name','value']
CSV_OUTPUT_NAME = {'norain': "../tables/mcdo_norain_17.1.csv",
                   'rain': "../tables/mcdo_rain_17.1.csv"}
ITZI_MAPS = {'norain': '1d_1000-17.1_wse_0001@swashes_1d_1000m',
             'rain': '1d_1000_rain_itzi-17.1_wse_0004@swashes_1d_1000m_rain'}
SWASHES_FILES = {'norain': '../simulations/swashes/1D_McDo_200.txt',
                 'rain': '../simulations/swashes/1D_McDo_rain_200.txt'}
VECTOR_POINTS = {'norain': 'axis_points@swashes_1d_1000m',
                 'rain': 'axis_points@swashes_1d_1000m_rain'}
# characteristics of SWASHES files
SWASHES_KEY = '#('  # start of header line
SWASHES_SEP = '\t'  # column separator


def read_swashes_file(swashes_file):
    """create a Pandas dataframe from the output of SWASHES
    """
    # find where starts the data
    with io.open(swashes_file, 'r') as f:
        for header_idx, line in enumerate(f):
            if SWASHES_KEY in line:
                break
    # read values
    swashes_df = pd.read_csv(swashes_file, header=header_idx,
                             sep=SWASHES_SEP, dtype=np.float32,
                             index_col=0)
    # list columns
    col_names = list(swashes_df)
    # remove spaces
    swashes_df.columns = [c.strip() for c in col_names]
    # remove necessary columns
    rm_list = ['h[i]', 'q[i]', 'Fr[i]=Froude',
               'topo[i]+hc[i]', 'Unnamed: 8', 'u[i]']
    swashes_df.drop(rm_list, axis=1, inplace=True)
    # rename columns
    swashes_df.columns = ['topo', 'wse']
    return swashes_df


def read_raster_values(key):
    """Return a Pandas series with raster values at given points
    """
    raster_map = ITZI_MAPS[key]
    vector_map = VECTOR_POINTS[key]
    raster_csv_raw = gscript.read_command("r.what", points=vector_map,
                                          map=raster_map, output="-",
                                          separator='comma', flags='n')
    # make the string readable by Pandas
    raster_csv = io.StringIO(unicode(raster_csv_raw))
    raster_df = pd.read_csv(raster_csv, dtype=np.float32,
                            index_col=0)
    # remove central columns
    col_names = list(raster_df)
    raster_df.drop(col_names[0:2], axis=1, inplace=True)
    # change column name
    raster_df.columns = ['itzi']
    # df to series
    raster_series = raster_df['itzi']
    return raster_series


def main():
    #~ for v in SWASHES_FILES.itervalues():
        #~ print(read_swashes_file(v).head())
    for k in ITZI_MAPS.iterkeys():
        raster_series = read_raster_values(k)
        swashes_df = read_swashes_file(SWASHES_FILES[k])
        # concatenate
        all_df = pd.concat([swashes_df, raster_series],  axis=1)
        # name index
        all_df.index.name = 'dx'
        # write to csv
        all_df.to_csv(CSV_OUTPUT_NAME[k])

if __name__ == "__main__":
    options, flags = gscript.parser()
    sys.exit(main())
