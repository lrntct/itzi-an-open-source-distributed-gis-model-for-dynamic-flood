#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
AUTHOR(S): Laurent Courty

PURPOSE:    Calculate indices for evaluating forecasting models by
            comparing forecasted and observed event.

COPYRIGHT: (C) 2016-2017 by Laurent Courty

            This program is free software; you can redistribute it and/or
            modify it under the terms of the GNU General Public License
            as published by the Free Software Foundation; either version 2
            of the License, or (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.
"""
from __future__ import division
import sys
import os
import pylatex as pl
import grass.script as grass
from grass.pygrass.messages import Messenger

THRES_MAPS = "out_nvar_drainvar_h_max_{t}"
THRES = [0.05, 0.10, 0.20, 0.30]

N_CORRESP = {"nvar": u"variable"}
N_LIST = N_CORRESP.keys()

OBS_CORRESP = {'flood_extend2@hull':'Both'}
rasters_observed = OBS_CORRESP.keys()
FILE_NAME = "scores_nvar_drainvar"

# start messenger
msgr = Messenger()


class Stats(object):
    """Provide methods to calculate and print statistics
    """
    def __init__(self, cell_count):
        self.cell_count = cell_count
        self.hits = cell_count['hits']
        self.fa = cell_count['false_alarms']
        self.misses = cell_count['misses']
        self.c_neg = cell_count['correct_neg']
        self.cell_sum = sum(cell_count.values())
        self.stats = {'accuracy': [None, "Accuracy"],
                      'bias': [None, "Bias score"],
                      'pod': [None, "Probability of detection"],
                      'far': [None, "False alarm ratio"],
                      'pofd': [None, "Probability of false detection"],
                      'sr': [None, "Success ratio"],
                      'ts': [None, "Critical success index"],  # critical success index
                      'ets': [None, "Equitable threat score"],  # Gilbert skill index
                      'hk': [None, "Hanssen & Kuipers discriminant"],
                      'hss': [None, "Heidke skill score"],
                      'or': [None, "Odds ratio"],
                      'orss': [None, "Odds ratio skill score"]}

    def calculate(self):
        """Calculate statistics and store them in a dictionary
        """
        self.stats['accuracy'][0] = (self.hits + self.c_neg) / self.cell_sum
        self.stats['bias'][0] = ((self.hits + self.fa) / (self.hits + self.misses))
        self.stats['pod'][0] = self.hits / (self.hits + self.misses)
        self.stats['far'][0] = self.fa / (self.hits + self.fa)
        self.stats['pofd'][0] = self.fa / (self.c_neg + self.fa)
        self.stats['sr'][0] = self.hits / (self.hits + self.fa)
        self.stats['ts'][0] = self.hits / (self.hits + self.misses + self.fa)
        hits_rd = (((self.hits + self.misses) * (self.hits + self.fa))
                   / self.cell_sum)
        self.stats['ets'][0] = ((self.hits - hits_rd)
                                / (self.hits + self.misses + self.fa - hits_rd))
        self.stats['hk'][0] = ((self.hits / (self.hits + self.misses))
                               - (self.fa / (self.c_neg + self.fa)))
        ec_rd = ((1 / self.cell_sum)
                 * ((self.hits + self.misses) * (self.hits + self.fa)
                    + (self.c_neg + self.misses) * (self.c_neg + self.fa)))
        self.stats['hss'][0] = ((self.hits + self.c_neg) - ec_rd) / (self.cell_sum - ec_rd)
        self.stats['or'][0] = (self.hits * self.c_neg) / (self.misses * self.fa)
        self.stats['orss'][0] = (((self.hits * self.c_neg) - (self.misses * self.fa)) /
            ((self.hits * self.c_neg) + (self.misses * self.fa)))

    def print_results(self):
        """Print results in a human readable way
        """
        print("{:>30}: {}".format("Hits", self.hits))
        print("{:>30}: {}".format("Misses", self.misses))
        print("{:>30}: {}".format("False alarms", self.fa))
        print("{:>30}: {}".format("Correct negatives", self.c_neg))
        for v in self.stats.values():
            print("{:>30}: {:.3f}".format(v[1], v[0]))

    def print_results_shell(self):
        """Print results in shell style
        """
        for k, v in self.cell_count.iteritems():
            print("{}={}".format(k, v))
        for k, v in self.stats.iteritems():
            print("{}={}".format(k, v[0]))


def get_cell_count(rast_forecast, rast_observed):
    """get results from r.stats
    """
    # set default to 0, in case one category is absent in results
    cell_count = {'hits':0, 'false_alarms':0,
                'misses':0, 'correct_neg':0}

    input_maps = rast_forecast + ',' + rast_observed
    res = grass.read_command("r.stats", flags="cn", input=input_maps)

    for line in res.strip().split(os.linesep):
        line = line.split()
        # line[0] = forecast, line[1] = observed
        bool_hits =         (line[0] == '1' and line[1] == '1')
        bool_false_alarms = (line[0] == '1' and line[1] == '0')
        bool_misses =       (line[0] == '0' and line[1] == '1')
        bool_correct_neg =  (line[0] == '0' and line[1] == '0')
        if bool_hits:
            cell_count['hits'] = int(line[2])
        elif bool_false_alarms:
            cell_count['false_alarms'] = int(line[2])
        elif bool_misses:
            cell_count['misses'] = int(line[2])
        elif bool_correct_neg:
            cell_count['correct_neg'] = int(line[2])
        else:
            msgr.fatal("Unknown posibility: {}".format(line))
    return cell_count


def gen_latex(results, file_name):
    doc = pl.Document()
    for observed, res_n in results.iteritems():
        for n, res in res_n.iteritems():
            with doc.create(pl.Table()) as table:
                caption = (u"Verification of the flood prediction in Hull "
                           u"using a {f} friction "
                           u"and the flooded area determined by {o}"
                           u"").format(f=N_CORRESP[n],
                                       o=OBS_CORRESP[observed])
                table.add_caption(caption)
                with doc.create(pl.Tabular('rllll')) as tabular:
                    tabular.add_hline()
                    tabular.add_row(('', pl.MultiColumn(4, align='c', data='Threshold depth'),))
                    thres_list = [t for t in THRES]
                    tabular.add_row(('',) + tuple(thres_list))
                    tabular.add_hline()
                    for score_name, treshold in res.iteritems():
                        #~ print score_name, treshold
                        tabular.add_row((score_name,
                                         treshold['0.05'], treshold['0.1'],
                                         treshold['0.2'], treshold['0.3']))
                    tabular.add_hline()
    doc.generate_tex(file_name)
    #~ doc.generate_pdf(file_name)


def get_stats():
    """
    - Create a dict like so: {thres:{score_name:score_value}}
    - Re-arrange to be {score_name:{thres:score_value}}
    - Encapsulate in {obs:{n:{score_name:{thres:score_value}}}}
    """
    results_all = {}
    for rast_observed in rasters_observed:
        results_n = {}
        for n in N_LIST:
            results_thres = {}
            for thres in THRES:
                rast_forecast = THRES_MAPS.format(t=thres)
                stats = Stats(get_cell_count(rast_forecast, rast_observed))
                stats.calculate()
                # score[1] is the name, score[0] the value
                results_thres[thres] = {score[1]:score[0] for score in stats.stats.itervalues()}
            # arrange to be {score_name:{thres:score_value}}
            scores_by_name = {}
            for scores in results_thres.itervalues():
                for score_name in scores:
                    scores_by_name[score_name] = {str(t):"{:.3f}".format(s[score_name]) for t, s in results_thres.iteritems()}
            results_n[n] = scores_by_name
        results_all[rast_observed] = results_n
    return results_all


def main():
    res = get_stats()
    #generate latex tables
    #~ print(res)
    gen_latex(res, FILE_NAME)


if __name__ == "__main__":
    options, flags = grass.parser()
    sys.exit(main())
