#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
# using tex in label

# Analytic
NORAIN = '../tables/mcdo_norain_17.1.csv'
RAIN = '../tables/mcdo_rain_17.1.csv'

# EA 2D T8a
EA2DT8A_RAIN = '../tables/ea2dt8a_rain.txt'
EA2DT8A_INFLOW = '../tables/ea2dt8a_inflow.txt'
LISFLOOD_RESULTS = '../tables/ea2dt8a_lisflood.txt'
ITZI171_RESULTS = '../tables/ea2dt8a_17.1.csv'
COL_NAMES =['Time (min)']+range(1,10)

# HULL
RAINFALL_HULL = '../tables/rainfall_hull.csv'

def ea2dt8a_depth_read():
    """Read values of water depth from text files.
    export a list of dataframes for each measuring point.
    """
    ea2dt8a_lisflood = pd.read_csv(LISFLOOD_RESULTS, sep='    ',
                                   header=15, index_col=0, engine='python',
                                   names=COL_NAMES)
    ea2dt8a_lisflood.index /= 60.
    # round entries
    ea2dt8a_lisflood.index = np.round(ea2dt8a_lisflood.index, 1)
    # read itzi results
    ea2dt8a_itzi = pd.read_csv(ITZI171_RESULTS, index_col=0, header=1,
                               names=COL_NAMES, na_values='*')
    ea2dt8a_itzi.fillna(0., inplace=True)
    ea2dt8a_itzi.index = pd.to_numeric(ea2dt8a_itzi.index)
    ea2dt8a_itzi.index /= 60.
    # compute differences
    diff = ea2dt8a_itzi - ea2dt8a_lisflood

    # Create one dataframe for each point, keeping only the eight first
    points_values = []
    for pt_idx in range(1,9):
        col_idx = [ea2dt8a_lisflood[pt_idx],
                   ea2dt8a_itzi[pt_idx],
                   diff[pt_idx]]
        col_keys = ['Lisflood', u'Itzï', 'Difference']
        new_df = pd.concat(col_idx,  axis=1, keys=col_keys)
        new_df.index.name = 'Time (min)'
        points_values.append(new_df)
    return points_values


def ea2dt8a_depth_plot(points_values):
    """Take a list of dataframes as entry and plot them
    """

    # define a grid 
    fig = plt.figure(figsize=(10,5.5))
    gs = gridspec.GridSpec(4, 4, height_ratios=[5,2,5,2],
                           wspace=0.03)
    # Put the gridspec in a 2d array
    gs_array = np.reshape([i for i in gs], (4,4))
    # Create empty python array
    axes_array = [[None for c in range(4)] for r in range(4)]
    # fill up the array with set axes
    for row_idx, gs_row in enumerate(gs_array):
        for col_idx, gs_elem in enumerate(gs_row):
            # if row is even, it where go depth values
            if row_idx % 2 == 0:
                # if the first element, set normally
                if col_idx == 0:
                    first_depth_axe = plt.subplot(gs_elem)
                    axes_array[row_idx][col_idx] = first_depth_axe
                # else, set the shared axe
                else:
                    axes_array[row_idx][col_idx] = plt.subplot(gs_elem,
                                                               sharex=first_depth_axe,
                                                               sharey=first_depth_axe)
            # if odd, it where go difference values
            else:
                # if the first element, set normally
                if col_idx == 0:
                    first_diff_axe = plt.subplot(gs_elem, sharex=first_depth_axe)
                    axes_array[row_idx][col_idx] = first_diff_axe
                # else, set the shared axe
                else:
                    axes_array[row_idx][col_idx] = plt.subplot(gs_elem,
                                                               sharex=first_depth_axe,
                                                               sharey=first_diff_axe)

    df_idx = 0
    # iterate in even rows (where goes depth values)
    for depth_row in axes_array[::2]:
        # each axe
        for col_idx, axe_depth in enumerate(depth_row):
            axe_depth.set_title("Point {}".format(df_idx + 1))
            # plot only depth values, not the differences
            df = points_values[df_idx]
            plot = df.plot(y=['Lisflood', u'Itzï'], style=['-', ','],
                           ax=axe_depth, colormap='Set1', legend=False)
            # set name of the y axis label
            plot.set_ylabel("Water depth (m)")
            # set limit in y
            axe_depth.set_ylim(0, 0.80)
            # Remove label where the legend is
            if col_idx in [1,2]:
                plot.set_xlabel('')
            # calculate RMSE
            rmse_mm = rmse_all(df[u'Itzï'], df['Lisflood']) * 1000
            axe_depth.annotate("RMSE: {:.1f} mm".format(rmse_mm),
                         (.02, .9), xycoords='axes fraction',
                          size=8)

            # bump index
            df_idx += 1

    # reset index
    df_idx = 0
    # iterate in odd rows (where goes differences values)
    for diff_row in axes_array[1::2]:
        # each axe
        for col_idx, axe_diff in enumerate(diff_row):
            # plot only depth values, not the differences
            df = points_values[df_idx]
            plot = df.plot(y=['Difference'],# colormap='Paired',
                           ax=axe_diff, legend=False)
            # set name of the y axis label
            plot.set_ylabel("Difference (m)")
            # set limit in y
            axe_diff.set_ylim(-0.1, 0.1)
            # Remove label where the legend is
            if col_idx in [1,2]:
                plot.set_xlabel('')
            # bump index
            df_idx += 1

    # General legend
    plt.figlegend(axe_depth.lines + axe_diff.lines,
                  ["LISFLOOD-FP", u"Itzï", u"Difference"],
                  loc = 'lower center', ncol=3, fancybox=True, frameon=True)
    #~ plt.show()
    plt.savefig('ea2dt8a_depth.pdf')
    plt.close()


def ea2dt8a_rain():
    ea2dt8a_rain = pd.read_csv(EA2DT8A_RAIN, sep='\t',
                                   header=0, index_col=0, squeeze=True)
    ea2dt8a_inflow = pd.read_csv(EA2DT8A_INFLOW, sep='\t',
                                   names=['Time (s)', u'Inflow in m³/s'],
                                   index_col=0, squeeze=True)
    fig, ax1 = plt.subplots()
    ea2dt8a_rain.plot(ax=ax1)
    ax1.fill_between(ea2dt8a_rain.index, 0, ea2dt8a_rain,
                     facecolor='blue', alpha=0.5)
    ax1.set_xlabel('Time (min)')
    ax1.set_ylabel(r'Rain ($\mathrm{mm}.\mathrm{h}^{-1}$)')
    ax1.set_ylim(900, 0)  # reverse the axe
    # add an axe
    ax2 = ax1.twinx()
    ea2dt8a_inflow.plot(ax=ax2)
    ax2.set_ylabel(r'Point inflow ($\mathrm{m}^{3}.\mathrm{s}^{-1}$)')
    ax2.set_ylim(0, 7)
    plt.tight_layout()
    plt.savefig('ea2dt8a_rain.pdf')
    plt.close()


def rmse(data):
    """return rmse from all but first element"""
    return np.sqrt(np.mean(data.itzi[1:] - data.wse[1:])**2)


def rmse_all(data, reference):
    """return rmse"""
    return np.sqrt(np.mean(data - reference)**2)


def macdonald():
    color_topo = 'black'
    color_topo_fill = '0.85'
    lw_topo = 0.5
    color_analytic = '0.4'
    color_sim = 'black'
    ls_sim = 'dashed'
    norain = pd.read_csv(NORAIN, sep=',',
                         header=0, index_col=0, squeeze=True)
    rain = pd.read_csv(RAIN, sep=',',
                       header=0, index_col=0, squeeze=True)
    lst = [(norain, u"Long channel without rain", 'mcdo_norain'),
           (rain, u"Long channel with rain", 'mcdo_rain')]
    for data, title, name in lst:
        plt.figure(figsize=(4,3))
        #~ plt.title(title)
        plt.xlabel('Length (m)')
        plt.ylabel('Height (m)')
        plt.plot(data.index, data.topo, color=color_topo, lw=lw_topo)
        plt.fill_between(data.index, data.topo, color=color_topo_fill)
        l_an, = plt.plot(data.index, data.wse, color=color_analytic,
                       label='Analytic solution')
        l_itzi, = plt.plot(data.index, data.itzi, linestyle=ls_sim,
                         color=color_sim, label=u'Itzï')
        plt.annotate("RMSE (m): {:.3f}".format(rmse(data)), (.65, .85),
                     xycoords='axes fraction', backgroundcolor='w', size=8)
        plt.legend([l_an, l_itzi], ["Analytic solution", u"Itzï"],
                      loc = 'upper right', ncol=2)
        plt.tight_layout()
        plt.savefig('{}.pdf'.format(name))
        plt.close()


def rainfall_hull():
    rain = pd.read_csv(RAINFALL_HULL, index_col=0, squeeze=True,
                       names=['Time', u'Intensity'])
    plt.bar(rain.index, rain, width=1)
    plt.xlabel('Time (hours)')
    plt.ylabel(r'Rainfall intensity (${mm}.{h}^{-1}$)')
    plt.tight_layout()
    plt.savefig('rainfall_hull.pdf')
    plt.close()


def main():
    sns.set(style="white", context='paper')
    sns.set_style({'lines.linewidth': 0.5, 'lines.linestyle': u'-',})
    df_list = ea2dt8a_depth_read()
    ea2dt8a_depth_plot(df_list)
    sns.set_context("poster", font_scale=2.2)
    ea2dt8a_rain()
    sns.set_context("paper")
    macdonald()
    sns.set_context("poster", font_scale=1.8)
    rainfall_hull()


if __name__ == "__main__":
    sys.exit(main())
